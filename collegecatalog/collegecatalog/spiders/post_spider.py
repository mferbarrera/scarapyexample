import scrapy
import csv

class GetSpider(scrapy.Spider):
    name = "cards"
    start_urls = [
        "https://www.schneideruniversities.com/catalog/college/1",
        "https://www.schneideruniversities.com/catalog/college/2"
    
    ]
    user_agent = 'Mozilla/5.0 (iPad; CPU OS 12_2 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Mobile/15E148'
    
    def parse(self,response):
        page = response.url.split('/')[-1]
        with open(f'page_{page}.html','wb') as f:
            f.write(response.body)
        dataset = []
        for li in response('li.ef-card'):
            data={}
            data['title']=li.css('div.h5').xpath('@title').extract()[0]
            data['url']=li.css('a.clamp-2').xpath('@href').extract()[0]
            data['stars']=len(li.css('span.fa-star.active'))/5
            dataset.append(data)
        dataset_cols = ['title','url','stars']
        print(dataset)
        with open('result.csv','w') as csvf:
            writer = csv.DictWriter(csvf,fieldnames=dataset_cols)
            writer.writeheader()
            for data in dataset:
                writer.writerow(data)